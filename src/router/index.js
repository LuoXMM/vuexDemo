import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routerList = [{
    path: '/vuex',
    component: () => import('../views/vuex/vuex')
}]
  
const router = new VueRouter({
    routes: routerList
})
export default router