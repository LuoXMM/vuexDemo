const state = {
    USER_TYPE_LIST: [
        {value: 1, label: '管理员'},
        {value: 2, label: '住户'},
        {value: 3, label: '业主'}
    ]
}
const getters = {
    USER_TYPE_LIST_USER_GETTER(state){
        const filterAry = state.USER_TYPE_LIST.filter(item =>{
            return item.value>2
        })
        return filterAry
    }
}
const mutations = {
    addUserType(state,obj){
        state.USER_TYPE_LIST = obj
    }
}
const actions = {
    editUserType({dispatch,commit,state},obj){
        // 此处可以调用后台接口，作异步数据处理。由于是demo原因，省略调用接口的功能
        // dispatch('xxx)   // 可以调用actions里面的其他方法
        // state可以获取state的值
        commit('addUserType',obj)
    }
}
export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}